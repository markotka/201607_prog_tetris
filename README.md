# README #

After reading https://www.quora.com/How-long-would-it-take-a-good-programmer-to-program-a-game-like-tetris-from-scratch I wanted to see how long does it take me to write a tetris clone. 

This took half a day, though I was doing some other tasks in parallel. Needs to be polished but the basic mechanisms are there.



### How do I get set up? ###

Should work on Mac and Linux, not on Windows (uses the ncurses library).

Compile it with g++ tetris.cpp -lncurses

### Who do I talk to? ###

marko.tkalcic@gmail.com