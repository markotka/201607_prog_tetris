#include <ncurses.h>
#include <unistd.h>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <sys/time.h>


int W = 10;
int H = 20;
int backgroundMap[20][10];
int pieceMap[4][4] = { {0,0,0,0}, {0,0,0,0}, {0,0,0,0}, {0,0,0,0} } ;
int pieces[][4][4] = { 
	{ {0,1,0,0}, {0,1,0,0}, {0,1,0,0}, {0,1,0,0}},
	{ {0,0,0,0}, {0,1,1,0}, {0,0,1,1}, {0,0,0,0}},
	{ {0,0,0,0}, {0,0,1,1}, {0,1,1,0}, {0,0,0,0}},
	{ {0,0,0,0}, {0,1,1,0}, {0,1,1,0}, {0,0,0,0}},
	{ {0,0,0,0}, {0,1,1,1}, {0,0,0,1}, {0,0,0,0}},
	{ {0,0,0,0}, {0,0,0,1}, {0,1,1,1}, {0,0,0,0}},
	{ {0,0,0,0}, {0,1,1,1}, {0,0,1,0}, {0,0,0,0}}
};
int currentPiece = 0;

int currentPieceHPosition = 0;
int currentPieceVPosition = 5;

void initialize(){
	for (int w = 0; w<W; w++){
		for (int h = 0; h < H; h++){
			backgroundMap[h][w] = 0;
		}
	}

	// screen
	initscr();
	cbreak(); // raw()
	noecho();
	halfdelay(1);

}

void render(){
	int boxLeftOffset = 20;
	int boxTopOffset = 5;

	// render the content
	for (int w = 0; w<W; w++){
		for (int h = 0; h < H; h++){
			if (backgroundMap[h][w] > 0){
				//mvaddch(boxTopOffset+h,boxLeftOffset+w,backgroundMap[h][w]+48);
				mvaddch(boxTopOffset+h,boxLeftOffset+w,'H');
			}
			else{
				mvaddch(boxTopOffset+h,boxLeftOffset+w,' ');
			}
		}
	}

	//render the frame
	for(int i = boxLeftOffset -1; i < boxLeftOffset + W + 1; i++){
		mvaddch(boxTopOffset,i,'*');
		mvaddch(boxTopOffset+H,i,'*');
	}
	for(int i = boxTopOffset; i < boxTopOffset + H + 1; i++){
		mvaddch(i,boxLeftOffset-1,'*');
		mvaddch(i,boxLeftOffset+W,'*');
	}
	refresh();
}

bool hasReachedTop(){
	bool isOnTop = false;
	for (int c = 0; c<W; c++){
		if (backgroundMap[4][c]>0){
			isOnTop = true;
		}
	}
	return isOnTop;
}

bool canMoveDown(){

	bool isThereSpace = true;
	for (int col = 0; col < W; col++){
		
		for (int line = 0; line < H-1; line++){
			mvprintw(6+line,5+col,"%d",backgroundMap[line+1][col]);
			if ( (backgroundMap[line+1][col] + backgroundMap[line][col]) == 3){
				isThereSpace = false;
			}
		}
		if (backgroundMap[H-1][col] == 2){
			isThereSpace = false;
		}

	}

	if (isThereSpace){
		return true;
	}
	else{
		return false;
	}
	
}

void clearPreviousPiece(){
	for (int w = 0; w<W; w++){
		for (int h = 0; h < H; h++){
			if (backgroundMap[h][w] == 2){
				backgroundMap[h][w] = 0;
			}
		}
	}
}

void setCurrentPiece(){
	for(int i = 0; i<4; i++){
		for(int j = 0; j<4; j++){
			pieceMap[i][j] = pieces[currentPiece][i][j];
		}
	}
}

void addNewPiece(){
	for(int i = 0; i<4; i++){
		for(int j = 0; j<4; j++){
			if (pieceMap[i][j] == 1){
				backgroundMap[currentPieceHPosition+i][currentPieceVPosition+j] = 2*pieceMap[i][j];
			}
		}
	}

}

void fixPieceToBackground(){
	for (int w = 0; w<W; w++){
		for (int h = 0; h < H; h++){
			if (backgroundMap[h][w] == 2){
				backgroundMap[h][w] = 1;
			}
		}
	}
}



void moveDown(){
	currentPieceHPosition++;
	clearPreviousPiece();
	addNewPiece();


}

void moveLeft(){
	bool isSpaceOnLeft = true;
	for (int l=currentPieceHPosition; l < currentPieceHPosition+4; l++){
		//mvprintw(5+l,35+currentPieceVPosition,"%d%d",backgroundMap[l][currentPieceVPosition],backgroundMap[l][currentPieceVPosition-1]);
		if (backgroundMap[l][0] == 2){
			isSpaceOnLeft = false;
		}
		for (int c = 0; c < W-1; c++){
			if (backgroundMap[l][c] == 1 && backgroundMap[l][c+1] == 2){
				isSpaceOnLeft = false;
			}
		}
	}
	if (isSpaceOnLeft){
		currentPieceVPosition--;
	}
}

void moveRight(){
	bool isSpaceOnRight = true;

	for (int l=currentPieceHPosition; l < currentPieceHPosition+4; l++){
		//mvprintw(5+l,35+currentPieceVPosition,"%d%d",backgroundMap[l][currentPieceVPosition],backgroundMap[l][currentPieceVPosition-1]);
		if (backgroundMap[l][W-1] == 2){
			isSpaceOnRight = false;
		}
		for (int c = 0; c < W-1; c++){
			if (backgroundMap[l][c] == 2 && backgroundMap[l][c+1] == 1){
				isSpaceOnRight = false;
			}
		}
	}
	
	if (isSpaceOnRight){
		currentPieceVPosition++;
	}
}

void rotate(){
	//todo: check if can rotate
	int tempPieceMap[4][4] = { {0,0,0,0}, {0,0,0,0}, {0,0,0,0}, {0,0,0,0} } ;
	for(int i=0; i<4; i++){
		for(int j = 0; j<4; j++){
			tempPieceMap[i][j] = pieceMap[i][j];
		}
	}
	for(int i=0; i<4; i++){
		for(int j = 0; j<4; j++){
			//pieceMap[i][j] = tempPieceMap[3-j][i];
			pieceMap[i][j] = tempPieceMap[j][3-i];
		}
	}
}

void clearLines(){
	for (int l = H-1; l > 0; l--){
		int sum = 0;
		for (int c=0;c<W;c++){
			sum += backgroundMap[l][c];
		}
		mvprintw(5+l,33,"%d/%d",sum,W);
		if (sum == W){
			for (int temp_l=l; temp_l > 1;temp_l--){
				for (int c=0;c<W;c++){
					backgroundMap[temp_l][c] = backgroundMap[temp_l-1][c];
				}
			}
		}
	}

}




int main(int argc, char const *argv[])
{
	/* code */
	initialize();

	struct timeval startTime;
	gettimeofday(&startTime, NULL);
	long sTime = startTime.tv_sec;


	int speed = 1;
	srand (time(NULL));

	long pausetime = (long) (100000 / (double) speed);

	bool not_over = true;


	// START

	int nextPiece = rand() % 7;
	struct timeval currentTime;
	long cTime, lmTime = 0;
	bool isTimeToMoveDown = true;
	bool not_landed = true;
	char cmd;
  
	while(not_over){
		currentPiece = nextPiece;
		nextPiece = rand() % 7;
		currentPieceVPosition = 0;
		currentPieceHPosition = 0;
		isTimeToMoveDown = true;
		not_landed = true;
		setCurrentPiece();
		
		//gettimeofday(&currentTime, NULL);
		
		
		while(not_landed){

			gettimeofday(&currentTime, NULL);
			//usleep(10);
			cTime = (currentTime.tv_sec-startTime.tv_sec) * 1000000 + currentTime.tv_usec;
			cmd = getch();
			mvprintw(1,1,"%c",cmd);
			switch(cmd){
				case '7': 
					moveLeft();
					clearPreviousPiece();
					addNewPiece();
				break;
				case '9': 
					moveRight();
					clearPreviousPiece();
					addNewPiece();
				break;
				case '8': 
					rotate();
					clearPreviousPiece();
					addNewPiece();
				break;

			}
			//moveLeft();
			//moveRight();
			if (isTimeToMoveDown){
				
				lmTime = cTime;
				if (canMoveDown()){
					moveDown();
				}
				else{

					fixPieceToBackground();
					clearLines();
					if (hasReachedTop()){
						not_over = false;
					}
					not_landed = false;
				}
				isTimeToMoveDown = false;
			}

			if ( cTime-lmTime > pausetime){
				//mvprintw(1,1,"YES: %ld",cTime-lmTime);
				isTimeToMoveDown = true;
			}
			render();
			
		} // while not_landed

		
	}





	endwin();
	return 0;
}